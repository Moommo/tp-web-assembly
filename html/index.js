/**
 * Class for charge the wasm
 */
const WasmLoader = class {
    constructor(path, refFunc) {
        this.path = path;
        this.refFunc = refFunc;
    }

    async importWasmInstance(path) {
        const rest = await fetch(path);
        const rawBytes = await rest.arrayBuffer();
        const module = new WebAssembly.Module(rawBytes);
        const instance = new WebAssembly.Instance(module);

        return instance;
    }

    async initFunctions(callback) {
        const Instance = await this.importWasmInstance(this.path);
        let functions = {};

        Object.keys(Instance.exports).forEach(func => {
            this.refFunc.forEach(keyFunc => {
                if(keyFunc == func) {
                    functions = {...functions, [keyFunc]: Instance.exports[func]};
                }
            });
        });

        callback(functions);
    }

    async run(callback) {
        const result = await this.initFunctions((functions)=> {
            callback(functions);
        });
    }
};


/**
 * Class for manage the calculator
 */
const Calculator = class {
    constructor(wasmLoader){
        this.operator = null;
        this.valueBefore = 0;
        this.valueAfter = 0;
        this.endCalcul = false;
        this.wasmLoader = wasmLoader;
    }

    setOperator(value) {
        this.operator = value;
    }

    removeLastValueBefore() {
        let newValue = this.valueBefore.slice(0, this.valueBefore.length - 1);
        this.valueBefore = newValue;
        console.log(this.valueBefore)
    }

    removeLastValueAfter() {
        let newValue = this.valueAfter.slice(0, this.valueAfter.length - 1);
        this.valueAfter = newValue;
        console.log(this.valueAfter)
    }

    getOperator(){
        return this.operator;
    }

    getValueBefore() {
        return this.valueBefore;
    }

    getValueAfter() {
        return this.valueAfter;
    }

    updatevalueBefore(value) {
        this.valueBefore += value;
    }

    updatevalueAfter(value) {
        this.valueAfter += value;
    }

    setDefaultValue(){
        this.operator = null;
        this.valueBefore = 0;
        this.valueAfter = 0;
    }

    isEndCalcul() {
        return this.endCalcul;
    }

    setEndCalcul(value) {
        this.endCalcul = value;
    }

    run(inputText) {
        this.wasmLoader.run((instance) => {
            const result = this.runOperator(instance);
            let finalResult = " = " + result;
            inputText.value += finalResult;
            this.setDefaultValue();
        });
        this.endCalcul = true;
    }

    runOperator(instance) {
        switch (this.operator) {
            case "+":
                return instance.addition(this.valueBefore, this.valueAfter);
                break;
            case "-":
                return instance.substraction(this.valueBefore, this.valueAfter);
                break;
            case "x":
                return instance.multiplication(this.valueBefore, this.valueAfter);
                break;
            case "÷":
                return instance.division(this.valueBefore, this.valueAfter);
                break;
            default:
                return 0;
        }
    }
    


}


/**
 * Manage element in the page
 */

const inputText = document.getElementById('info-calculate');
const wasmLoader = new WasmLoader('calculator.wasm',['addition', 'substraction', 'multiplication', 'division']);
const calculator = new Calculator(wasmLoader);


function appendDisplay(value) {
    if (calculator.isEndCalcul()) {
        inputText.value = value;
        calculator.setEndCalcul(false);
    } else {
        inputText.value += value;
    }
    
    if (calculator.getOperator() == null) {
        calculator.updatevalueBefore(value);
    }
    else {
        calculator.updatevalueAfter(value);
    }
}

function appendDisplayOperator(value) {
    calculator.setOperator(value);
    inputText.value += value;
    disableButtonOperator();
    enableButtonAction();
}

function cleanAllInput() {
    inputText.value  = '';
    calculator.setDefaultValue();
}

function cleanLastElement() {
    if (calculator.getOperator() == null && calculator.getValueAfter() == 0) {
        calculator.removeLastValueBefore();
    }
    else {
        if (calculator.getValueBefore() != 0 && calculator.getOperator() != null) {
            calculator.removeLastValueAfter();
        } else {
            calculator.setOperator(null);
            enableButtonOperator();
        }
    }
    let actualValue = inputText.value;
    let newValue = actualValue.slice(0, actualValue.length - 1);
    inputText.value = newValue;
}


/**
 * MAIN 
 */

const actionButton = document.getElementById('makeCalcul');
const operatorButtons = document.querySelectorAll('.touch-action');

function enableButtonAction() {
    actionButton.classList.remove('passive');
}

function disableButtonAction() {
    actionButton.classList.add('passive');
}

function enableButtonOperator() {
    operatorButtons.forEach(button => {
        button.classList.remove('passive');
    });
}

function disableButtonOperator() {
    operatorButtons.forEach(button => {
        button.classList.add('passive');
    });
}


function runCalculus() {
    calculator.run(inputText);
    disableButtonAction();
    enableButtonOperator();
}
